BUILD_ARCH ?= $(shell uname -m)
SOURCE_DIR = $(patsubst %/,%,${PWD})
BINARY_DIR = .build/$(BUILD_ARCH)
RELEASE_BINARY_DIR = .build-release/$(BUILD_ARCH)
CLANG_BINARY_DIR = .build-clang/$(BUILD_ARCH)
QMAKE_FLAGS ?= "CONFIG += c++11 debug silent warn-on"

%: qmake
	cd ${BINARY_DIR}; ${MAKE} ${MAKE_FLAGS} $@

all: qmake
	cd ${BINARY_DIR}; ${MAKE} ${MAKE_FLAGS} all

qmake:
	mkdir -p ${BINARY_DIR}; cd ${BINARY_DIR}; qmake ${QMAKE_FLAGS} ${SOURCE_DIR}

install: qmake
	cd ${BINARY_DIR}; ${MAKE} ${MAKE_FLAGS} install

clean: qmake clean-release
	cd ${BINARY_DIR}; ${MAKE} ${MAKE_FLAGS} clean

.PHONY: qmake all install clean update-po update-gmo


all-release: qmake-release
	cd ${RELEASE_BINARY_DIR}; ${MAKE} ${MAKE_FLAGS} all

qmake-release:
	mkdir -p ${RELEASE_BINARY_DIR}; cd ${RELEASE_BINARY_DIR}; qmake ${SOURCE_DIR}

clean-release: qmake-release
	cd ${RELEASE_BINARY_DIR}; ${MAKE} ${MAKE_FLAGS} clean

.PHONY: qmake-release clean-release all-release