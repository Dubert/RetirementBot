TEMPLATE = app

QT += network widgets core

QMAKE_CXXFLAGS += -Wall -O2

CONFIG += c++17 console c++1z

TARGET = RetirementBot

VERSION = 0.0.2
DEFINES += APP_VERSION=\\\"v$$VERSION\\\"
DEFINES += APP_NAME=\\\"SaintBot\\\"

SOURCES += src/main.cpp \
           src/ircchat/ircchat.cpp \
           src/ircchat/httpserver.cpp \
           src/ircchat/networkmanager.cpp \
           src/ircchat/apibase.cpp \
           src/ircchat/spotify.cpp \
           src/commandline.cpp

HEADERS += src/ircchat/ircchat.h \
           src/ircchat/httpserver.h \
           src/ircchat/networkmanager.h \
           src/ircchat/client_info.h \
           src/ircchat/apibase.h \
           src/ircchat/spotify.h \
           src/commandline.h
