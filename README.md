# Retirement Bot

Qt/C++ twitch bot

####

install compiler and qt

arch example: sudo pacman -S gcc qt5-base

#### download src from github and build

* git clone https://gitlab.com/dubert/retirementbot
* cd retirementbot
* make


### to use spotify and riot api, edit the following values
edit irc/ircchat/client_info.h, can be left empty
* SPOTIFY_ID your spotify id
* SPOTIFY_SECRET spotify secret api key
* REDIRECT_URL spotify redirect url, your ip address or address where the bot is running
* RIOT_ID  # riot api key


####

todo:
  
  add ability to change number of messages inbetween repeated commands

  customizable welcome sub message
