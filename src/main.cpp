//do whatever with this code,
//but qt beaware qt probably has some licenses.

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QUrl>

#include "ircchat/ircchat.h"
#include "commandline.h"

#include <csignal>

int main(int argc, char *argv[])
{
  for(auto sig : { SIGINT, SIGHUP, SIGTERM }) {
    std::signal(sig, [](int) { qApp->quit(); });
  }

  QCoreApplication app(argc, argv);

  app.setApplicationVersion(APP_VERSION);

  CommandLine commandline(&app);
  // Load app settings
  // todo add settings
  //SettingsManager::getInstance()->load();
  IrcChat chatBot(&commandline);

  // Start
  return app.exec();
}
