#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDesktopServices>
#include <QNetworkConfiguration>
#include <QNetworkConfigurationManager>
#include <QNetworkInterface>
#include <QEventLoop>

#include "networkmanager.h"
#include "client_info.h"

const quint16 NetworkManager::PORT = 443;
const QString NetworkManager::HOST = "irc.chat.twitch.tv";
const QString NetworkManager::Kraken = "https://api.twitch.tv/kraken";
const QString NetworkManager::CLIENT_ID = "da2q1g48cq91ge3owcgww93d2mdkk3";
const QString NetworkManager::RiotGames = "https://na1.api.riotgames.com";
const QString NetworkManager::summoner_v4 = "/lol/summoner/v4/summoners/by-name/";
const QString NetworkManager::spectator_v4 = "/lol/spectator/v4/active-games/by-summoner/";
const QString NetworkManager::league_v4 = "/lol/league/v4/positions/by-summoner/";
const QString NetworkManager::static_data_v3 = "/lol/static-data/v3/";

NetworkManager::NetworkManager(QObject* parent)
  : QObject(parent)
  , _sock(std::make_unique<QSslSocket>(this))
  , _settings("retirementbot.app", "RetirementBot")
  , _spotify(&_settings, &man)
{
  if(!_settings.contains("authtoken") || !_settings.contains("spotifytoken")) {
    _server.start();

    connect(
        &_server,
        &HttpServer::codeReceived,
        [&](QString code, int type) {
          switch(type) {
            // TODO: make this a real enum
            //twitch auth
            case 0:
            {
              _settings.setValue("authtoken", code);
              _authKey = code;
              getUser();
              break;
            }
            //twitch refresh token
            case 2:
            {
              // not used with kraken api
              // TODO: migrate to helix
              _settings.setValue("twitch_rt", code);
              break;
            }
            //spotify auth
            case 1:
            {
              _spotify.refreshRequest(
                  "code="+code+"&redirect_uri=" REDIRECT_URL,
                  "authorization_code");
              break;
            }
            default:
              break;
          }
        });

    /*
    QUrl url( "https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=" + CLIENT_ID
            + "&redirect_uri=http://localhost:8979"
            + "&scope=user_read%20chat_login%20user_blocks_read%20user_blocks_edit"
            + "&force_verify=true");

    QDesktopServices::openUrl(url);
    */
  }
  if(_settings.contains("authtoken")) {
    _authKey = _settings.value("authtoken").toString();
    getUser();
  }

  if(!_sock) {
    qDebug() << "error creating socket!";
  }
  else {
    _sock->setPeerVerifyMode(QSslSocket::VerifyPeer);
    connect(
        _sock.get(),
        &QSslSocket::readyRead,
        this,
        &NetworkManager::receive);

    connect(
        _sock.get(),
        static_cast<void (QSslSocket::*)(QAbstractSocket::SocketError)>(&QSslSocket::error),
        this,
        &NetworkManager::processError);

    connect(
        _sock.get(),
        static_cast<void (QSslSocket::*)(const QList<QSslError> &errors)>(&QSslSocket::sslErrors),
        this,
        &NetworkManager::processSslErrors);

    connect(
        _sock.get(),
        &QSslSocket::encrypted,
        this,
        &NetworkManager::login);

    connect(
        _sock.get(),
        &QSslSocket::encrypted,
        this,
        &NetworkManager::onSocketStateChanged);

    connect(
        _sock.get(),
        &QSslSocket::disconnected,
        this,
        &NetworkManager::onSocketStateChanged);
  }
  //Select interface
  connectionOK = false;
  testNetworkInterface();

  //Set up offline poller
  offlinePoller.setInterval(2000);
  connect(&offlinePoller, &QTimer::timeout, this, &NetworkManager::testNetworkInterface);

  getRiotVersionInfo();
}

void NetworkManager::send_ping()
{
  if (!sendPing)
  {
    sendPing = std::make_unique<QTimer>();
    // every 3 minutes check
    sendPing->setInterval(180000);

    connect(sendPing.get(), &QTimer::timeout, [&]() {
      sendMessage("PING");
      qDebug() << "sending ping";
      if (!_receivedPong)
      {
        offlinePoller.start();
        testNetworkInterface();
      }
    });
  }

  sendPing->start();
  _receivedPong = false;
}

void NetworkManager::testConnection()
{
  QNetworkRequest request;

  request.setRawHeader("Accept", "application/vnd.twitchtv.v5+json");
  request.setRawHeader("Client-ID", CLIENT_ID.toUtf8());
  request.setUrl(QUrl(Kraken));

  QNetworkReply *reply = man.get(request);

  connect(reply, &QNetworkReply::finished, this, &NetworkManager::testConnectionReply);
}

void NetworkManager::testConnectionReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  handleNetworkError(reply);

  emit finishedConnectionTest();
}

void NetworkManager::testNetworkInterface()
{
  //Chooses a working network interface from interfaces list, if default configuration doesn't work
  QNetworkConfigurationManager conf;
  QString identifier;

  QEventLoop loop;
  connect(this, &NetworkManager::finishedConnectionTest, &loop, &QEventLoop::quit);

  //Test default configuration
  man.setConfiguration(conf.defaultConfiguration());

  testConnection();
  loop.exec();

  if (connectionOK) {
    if (!connected())
    {
      qDebug() << "Selected default network configuration";
      reopenSocket();
    }
    offlinePoller.stop();
    send_ping();
    return;
  }
  else
  {
    qDebug() << "Failure on default configuration, attempt to choose another interaface..";

    foreach (const QNetworkInterface & interface, QNetworkInterface::allInterfaces())
    {
      if (!interface.isValid())
          continue;

//            qDebug() << "Identifier: " << interface.name();
//            qDebug() << "HW addr: " << interface.hardwareAddress();

      bool isUp = interface.flags().testFlag(QNetworkInterface::IsUp);
      bool isLoopback = interface.flags().testFlag(QNetworkInterface::IsLoopBack);
      bool isActive = interface.flags().testFlag(QNetworkInterface::IsRunning);
//            bool isPtP = interface.flags().testFlag(QNetworkInterface::IsPointToPoint);

//            qDebug() << "Properties: ";
//            qDebug() << (isUp ? "Is up" : "Is down");

//            if (isLoopback)
//                qDebug() << "Loopback";

//            qDebug() << (isActive ? "Active" : "Inactive");

//            if (isPtP)
//                qDebug() << "Is Point-to-Point";

//            qDebug() << "";

      if (isUp && isActive && !isLoopback)
      {
        identifier = interface.name();
        qDebug() << "Testing connection for interface " << identifier;
        man.setConfiguration(conf.configurationFromIdentifier(identifier));

        testConnection();
        loop.exec();

        if (connectionOK)
        {
          qDebug() << "Success!";
          if (!connected())
          {
            reopenSocket();
          }
          offlinePoller.stop();
          send_ping();
          return;
        }
        else
          qDebug() << "Failure, trying another interface...";
      }
    }
  }

  if (!connectionOK) {
    man.setConfiguration(conf.defaultConfiguration());
    disconnect();
  }
}


NetworkManager::~NetworkManager()
{
  disconnect();
}

bool NetworkManager::connected()
{
  if (_sock) {
    return _sock->state() == QTcpSocket::ConnectedState;
  }
  else {
    return false;
  }
}

void NetworkManager::reopenSocket() {
  if (_sock) {
    qDebug() << "Reopening socket";
    if (_sock->isOpen()) {
      qDebug() << "closing socket";
      _sock->close();
    }
    _sock->open(QIODevice::ReadWrite);
    _sock->connectToHostEncrypted(HOST, PORT);
    if (!_sock->isOpen()) {
      qDebug() << "Error opening socket";
    }
  }
}

void NetworkManager::disconnect() {
  if (_sock) {
    _sock->close();
  }
  emit disconnected();
}

void NetworkManager::receive() 
{
  QString msg;
  while (_sock && _sock->canReadLine()) {
    msg = _sock->readLine();
    msg = msg.remove("\n").remove("\r");
    if (msg.startsWith("PONG "))
    {
      qDebug() << "received pong";
      _receivedPong = true;
      send_ping();
      return;
    }
    emit recievedMessage(msg);
  }
}

void NetworkManager::sendMessage(const QString& msg)
{
  QString replyMsg = msg + "\r\n";
  _sock->write(replyMsg.toStdString().c_str());
}

//-------------------------------------------------------------
// handleNetworkError
//-------------------------------------------------------------
bool NetworkManager::handleNetworkError(QNetworkReply* reply)
{
  if (reply->error() != QNetworkReply::NoError)
  {
    if (reply->error() >= 1 && reply->error() <= 199)
    {
      if (connectionOK == true)
      {
        connectionOK = false;
      }
    }

    qDebug() << reply->errorString();

    return false;
  }

  if (!connectionOK) {
    connectionOK = true;
  }

  return true;
}

void NetworkManager::processError(QAbstractSocket::SocketError socketError) {
  QString err;
  switch (socketError) {
  case QAbstractSocket::RemoteHostClosedError:
    err = "Server closed connection.";
    break;
  case QAbstractSocket::HostNotFoundError:
    err = "Host not found.";
    break;
  case QAbstractSocket::ConnectionRefusedError:
    err = "Connection refused.";
    break;
  case QAbstractSocket::SocketTimeoutError:
    err = "Socket Timed out.";
    break;
  case QAbstractSocket::NetworkError:
    err = "Network error.";
    break;
  default:
    err = "Unknown error.";
    break;
  }
  qDebug() << "socket error: " << err << socketError;
  if (!connected())
  {
    reopenSocket();
  }
}

void NetworkManager::processSslErrors(const QList<QSslError> &errors) {
  for (const auto & error : errors) {
    qDebug() << error.errorString();
  }
}

void NetworkManager::onSocketStateChanged() {
  // We don't check if connected property actually changed because this slot should only be awaken when it did
}

/********************************************************************
 *
 *  getUser()
 *
 ********************************************************************/
void NetworkManager::getUser()
{
  qDebug() << "getting user";

  QString url = Kraken + "/user";
  QString auth = "OAuth " + _authKey;

  QNetworkRequest request;
  request.setRawHeader("Accept", "application/vnd.twitchtv.v5+json");
  request.setRawHeader("Client-ID", CLIENT_ID.toUtf8());
  request.setUrl(QUrl(url));
  request.setRawHeader(QString("Authorization").toUtf8(), auth.toUtf8());

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::userReply);
}

/********************************************************************
 *
 *  userReply()
 *
 ********************************************************************/
void NetworkManager::userReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  qDebug() << "got user reply";
  //if (!handleNetworkError(reply)) {
  //    return;
  //}
  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data,&error);

  quint64 userId;
  QString displayName;
  if (error.error == QJsonParseError::NoError) {
    QJsonObject json = doc.object();
    if (!json["name"].isNull())
      displayName = json["name"].toString();
    userId = json["_id"].toString().toULongLong();
  }
  qDebug() << "user name and id" << displayName << userId;

  emit recievedUserInfo(displayName, userId, UserType::AuthUser);
  reply->deleteLater();
}

/********************************************************************
 *
 *  getTwitchUser()
 *    get twitch user(s) sperated by a ','
 ********************************************************************/
void NetworkManager::getTwitchUser(const QString& users)
{
  qDebug() << "getting user";

  QString url = Kraken + "/users?login=" + QUrl::toPercentEncoding(users);

  QNetworkRequest request;
  request.setRawHeader("Accept", "application/vnd.twitchtv.v5+json");
  request.setRawHeader("Client-ID", CLIENT_ID.toUtf8());
  request.setUrl(QUrl(url));

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::twitchUsersReply);
}

/********************************************************************
 *
 *  userReply()
 *
 ********************************************************************/
void NetworkManager::twitchUsersReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data,&error);

  quint64 userId;
  QString displayName;
  if (error.error == QJsonParseError::NoError) {
    QJsonObject json = doc.object();
    for(auto users : json["users"].toArray())
    {
      auto userObj = users.toObject();
      displayName = userObj["name"].toString();
      userId = userObj["_id"].toString().toULongLong();
    }
  }
  qDebug() << "user name and id" << displayName << userId;

  emit recievedUserInfo(displayName, userId, UserType::StreamUser);
  reply->deleteLater();
}

/********************************************************************
 *
 *  getTwitchUser()
 *    get twitch stream info by id
 ********************************************************************/
void NetworkManager::requestUserStream(const quint64 id)
{
  qDebug() << "getting stream id " << id;

  QString url = Kraken + "/streams/" + QString::number(id);

  QNetworkRequest request;
  request.setRawHeader("Accept", "application/vnd.twitchtv.v5+json");
  request.setRawHeader("Client-ID", CLIENT_ID.toUtf8());
  request.setUrl(QUrl(url));

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::twitchStreamReply);
}

/********************************************************************
 *
 *  userReply()
 *
 ********************************************************************/
void NetworkManager::twitchStreamReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data,&error);
  qDebug() << "twitch stream reply" << doc; // << error;
  qint64 seconds = 0;
  if (error.error == QJsonParseError::NoError)
  {
    QJsonObject json = doc.object();

    //Online streams
    auto stream = json["stream"].toObject();
    if (!stream.empty()) {
      auto started = stream["created_at"].toString();
      qDebug() << "stream created at: " << started.left(started.size() -1);
      auto t = QDateTime::fromString(started, Qt::ISODate);
      seconds = t.secsTo(QDateTime::currentDateTimeUtc());
      qDebug() << "stream time; " << (int)((seconds / (60 * 60)) % 24) << (int)((seconds / 60) % 60) << (int)(seconds) % 60;
    }
  }
  
  emit recievedStreamUptime(seconds);
  reply->deleteLater();
}

/********************************************************************
 *
 *  getSummonerID()
 *
 ********************************************************************/
void NetworkManager::getSummonerID(const QString& summonerName)
{
  qDebug() << "getting summoner id";

  QString url = RiotGames + summoner_v4 + summonerName + "?api_key=" + RIOT_ID;
  qDebug() << url;

  QNetworkRequest request;

  request.setUrl(QUrl(url));

  QNetworkReply *reply = man.get(request);

  connect(
    reply, 
    &QNetworkReply::finished,
    this,
    &NetworkManager::summonerReply);
}

/********************************************************************
 *
 *  summonerReply()
 *
 ********************************************************************/
void NetworkManager::summonerReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  qDebug() << "got summoner reply";
  //if (!handleNetworkError(reply)) {
  //    return;
  //}
  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data, &error);

  QString summonerId;
  if (error.error == QJsonParseError::NoError)
  {
    QJsonObject json = doc.object();
    if (!json["id"].isNull())
      summonerId = json["id"].toString();
  }
  qDebug() << "summoner id" << summonerId;
  emit summoner_id(summonerId);

  reply->deleteLater();
}

/********************************************************************
 *
 *  getSummonerGameInfo()
 *
 ********************************************************************/
void NetworkManager::getSummonerGameInfo(const QString& summonerID)
{
  _requestedId = summonerID;
  QString url = RiotGames + spectator_v4 + summonerID + "?api_key=" + RIOT_ID;
  qDebug() << "url for spectator: " << url;

  QNetworkRequest request;

  request.setUrl(QUrl(url));

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::gameInfoReply);
}

/********************************************************************
 *
 *  gameInfoReply()
 *
 ********************************************************************/
void NetworkManager::gameInfoReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  qDebug() << "got game info reply";
  //if (!handleNetworkError(reply)) {
  //    return;
  //}
  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data, &error);

  bool foundGame = false;
  if (error.error == QJsonParseError::NoError) {
    QJsonObject json = doc.object();
    for(auto summoners : json["participants"].toArray()) {
      auto summonerId = summoners.toObject()["summonerId"].toString();
      if(_requestedId == summonerId)
      {
        foundGame = true;
        QString runeInfo;
        auto perks = summoners.toObject()["perks"].toObject();
        runeInfo = "Runes: " +  _runes[perks["perkStyle"].toInt()] + " secondary rune: " + _runes[perks["perkSubStyle"].toInt()] + " selected:";
        for(auto runeID : perks["perkIds"].toArray()) {
          runeInfo += " " + _runes[runeID.toInt()];
        }
        emit runesInfo(runeInfo);
      }
    }
  }
  _requestedId.clear();

  if(!foundGame) {
    emit runesInfo("invalid");
  }

  reply->deleteLater();
}

/********************************************************************
 *
 *  getSummonerGameInfo()
 *
 ********************************************************************/
void NetworkManager::getSummonerRank(const QString& summonerID)
{
  _requestedId = summonerID;
  QString url = RiotGames + league_v4 + summonerID + "?api_key=" + RIOT_ID;
  qDebug() << "url for league: " << url;

  QNetworkRequest request;

  request.setUrl(QUrl(url));

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::rankInfoReply);
}

/********************************************************************
 *
 *  gameInfoReply()
 *
 ********************************************************************/
void NetworkManager::rankInfoReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  //if (!handleNetworkError(reply)) {
  //    return;
  //}
  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data, &error);

  QString response;
  if (error.error == QJsonParseError::NoError) {
    QJsonArray json = doc.array();
    qDebug() << "got league info reply" << json;
    for(auto positions : json)
    {
      auto posObject = positions.toObject();
      qDebug() << posObject;
      if (response.isEmpty())
      {
        response += posObject["summonerName"].toString() += ": ";
      }
      response += posObject["position"].toString() += " ";
      response += posObject["tier"].toString() += " ";
      response += posObject["rank"].toString() += " ";
      response += QString::number(posObject["leaguePoints"].toInt()) += " ";
    }
  }

  emit rankInfo(response);

  reply->deleteLater();
}

/********************************************************************
 *
 *  getRiotVersionInfo()
 *
 ********************************************************************/
void NetworkManager::getRiotVersionInfo()
{
  qDebug() << "getting version info";

  QNetworkRequest request;

  request.setUrl(QUrl("http://ddragon.leagueoflegends.com/api/versions.json"));

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::riotVersionReply);
}

/********************************************************************
 *
 *  riotVersionReply()
 *
 ********************************************************************/
void NetworkManager::riotVersionReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  qDebug() << "got version reply";
  //if (!handleNetworkError(reply)) {
  //    return;
  //}
  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data,&error);

  if (error.error == QJsonParseError::NoError) {
    QJsonArray json = doc.array();
    for(auto version : json) {
      _riotVersion = version.toString();
      break;
    }
  }

  reply->deleteLater();
  getRuneInfo();
}

/********************************************************************
 *
 *  getRuneInfo()
 *
 ********************************************************************/
void NetworkManager::getRuneInfo()
{
  qDebug() << "getting rune info";

  QString url = "http://ddragon.leagueoflegends.com/cdn/" + _riotVersion + "/data/en_US/runesReforged.json";
  qDebug() << "riot url" << url;

  QNetworkRequest request;

  request.setUrl(QUrl(url));

  QNetworkReply *reply = man.get(request);

  connect(
      reply, 
      &QNetworkReply::finished,
      this,
      &NetworkManager::runesReply);
}

/********************************************************************
 *
 *  summonerReply()
 *
 ********************************************************************/
void NetworkManager::runesReply()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  qDebug() << "got rune reply";
  //if (!handleNetworkError(reply)) {
  //    return;
  //}
  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data,&error);

  if (error.error == QJsonParseError::NoError) {
    QJsonArray json = doc.array();
    for(auto runes : json) {
      auto runeName = runes.toObject()["name"].toString();
      auto runeID = runes.toObject()["id"].toInt();
      _runes[runeID] = runeName;
      auto runeArray = runes.toObject()["slots"].toArray();
      for(auto subRune : runeArray) {
        for(auto runeSlot : subRune.toObject()["runes"].toArray()) {
          _runes[runeSlot.toObject()["id"].toInt()] = runeSlot.toObject()["name"].toString();
        }
      }
    }
  }

  reply->deleteLater();
}
