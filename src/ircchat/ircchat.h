#ifndef IRCCHAT_H
#define IRCCHAT_H

#include <QSettings>
#include <QTimer>

#include "../commandline.h"
#include "networkmanager.h"

#include <memory>

class Tags
{
  QMap<QString, QString> tags;

public:
  Tags(const QString& msg);
  QString getTag(const QString& tag);
};

class Command : public QObject
{
  Q_OBJECT

private:
  std::unique_ptr<QTimer> fireTime;
  QString message;
  bool needsSending = false;

signals:
  void commandReady(Command* cmd);

public:

  Command(int time, const QString& msg, ...) : message(msg) {
    if(time < 60000) {
      time = 300000;
    }

    fireTime = std::make_unique<QTimer>();
    fireTime->setInterval(time);
    fireTime->setSingleShot(true);
    fireTime->start();

    connect(fireTime.get(), &QTimer::timeout, [&]() {
      commandReady(this);
    });
  }

  QString getMessage() const {
    return message;
  }

  int getInterval() const {
    return fireTime->interval();
  }

  bool checkSendingStatus() const {
    return needsSending;
  }

  void needStatus(bool status) {
    needsSending = status;
  }

  void resetCommand() {
    fireTime->start();
    needsSending = false;
  }
};

class IrcChat : public QObject
{
  Q_OBJECT

  bool _loggedIn = false;
  QString _authKey = "";
  // room for bot to join
  QString _room = "saintvicious";
  QString _username = "";
  QString _userpass = "";
  QString _displayName = "";
  quint64 _userId = 0;
  bool _anonym;
  QString _roomChannelId;
  QSettings _settings;
  std::unique_ptr<NetworkManager> _nm;

  QMap<QString, QString> _commands;
  // twitch user info
  struct userInfo {
    QString _displayName;
    quint64 _id;
    UserType type;
  };
  QVector<userInfo> _otherUsers;

  int _messagesInbetween = 50;
  int _messagesSeen = 0;
  void checkRepeaters();
  QMap<QString, Command*> _repeatCommands;

  QSet<QString> _authorized;
  QSet<QString> _permittedUsers;
  qint32 _requestedId = 0;
  qint32 _rankI = 0;
  std::vector<QString> _summonerIDs {};

public:
  IrcChat(CommandLine* cmd, QObject * parent = 0);
  ~IrcChat();

signals:
  void sendMessage(const QString& msg);

public slots:
  void login();

private slots:
  void parseCommand(QString cmd);
  void sendRepeatedMessage(Command* cmd);

private:
  void setAnonymous(bool newAnonymous);
  void join(const QString channel, const QString channelId);
  void disconnect();
  void leave();
  void loadSettings();
  void saveSettings();
  void sendRoomMessage(const QString& msg);
};

#endif //IRCCHAT_H
