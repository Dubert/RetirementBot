#pragma once

#include "apibase.h"

#include <memory>

class Spotify : public ApiBase {
  Q_OBJECT
  DECLARE_API_BASE(Spotify)

  QString _spotifyKey;
  QString _refreshToken;
  QString _accessToken;
  std::unique_ptr<QByteArray> _data;

  static const QString SpotifyToken;

private slots:
  void receivedRefreshToken();
  void receivedSong();

public slots:
  void keyExpired();

signals:
  void songInfo(const QString& songName, const QString& artist);

 public:
  void songRequest() const;
  void refreshRequest(const QString& key, const QString& type);
};