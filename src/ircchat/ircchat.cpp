#include <QDebug>
#include <QUrl>
#include <QRegExp>
#include <tuple>

#include "ircchat.h"

Tags::Tags(const QString& msg)
{
  int tagsEnd = msg.indexOf(" ");
  QString onlyTags = msg.mid(1, tagsEnd - 1);
  QStringList tagList = onlyTags.split(';');
  for(auto tag : tagList) {
    QStringList tagParts = tag.split("=");
    if(tagParts.count() > 1) {
      tags.insert(tagParts[0], tagParts[1]);
    }
  }
}

QString Tags::getTag(const QString& tag)
{
  return tags[tag];
}

IrcChat::IrcChat(CommandLine* cmd, QObject * parent)
  : QObject(parent)
  , _settings("retirementbot.app", "RetirementBot")
  , _nm(std::make_unique<NetworkManager>())
{
  connect(
      this,
      &IrcChat::sendMessage,
      _nm.get(),
      &NetworkManager::sendMessage);

  connect(
      _nm.get(),
      &NetworkManager::login,
      this,
      &IrcChat::login);

  connect(
      _nm.get(),
      &NetworkManager::recievedUserInfo,
      [&](const QString& displayName, quint64 id, UserType type) {
        switch (type) {
          case UserType::AuthUser:
            _displayName = displayName;
            login();
            break;
          case UserType::StreamUser:
            _otherUsers.append({ displayName, id, type});
            _nm->requestUserStream(id);
            break;
          case UserType::OtherUser:
            _otherUsers.append({ displayName, id, type});
            break;
        }
      });

  connect(
    _nm.get(),
    &NetworkManager::recievedStreamUptime,
    [&](qint64 seconds) {
      qint64 time = 900000;
      QString message = QString("VoHiYo You've been streaming for %1, you should have drank %2 gallons of Cure Potions WanWan").arg(seconds);
      Command* repeat = new Command(time, message);
      connect(repeat, &Command::commandReady, this, &IrcChat::sendRepeatedMessage);
  
      auto fireTime = std::make_unique<QTimer>();
      fireTime->setInterval(time);
      fireTime->setSingleShot(true);
      fireTime->start();

      connect(fireTime.get(), &QTimer::timeout, [&]() {
        _repeatCommands.insert("uptime", repeat);
        fireTime->deleteLater();
      });
    }
  );

  connect(
      _nm.get(),
      &NetworkManager::recievedMessage,
      this,
      &IrcChat::parseCommand);

  connect(
      &_nm->getSpotify(),
      &Spotify::songInfo,
      [&](const QString& song, const QString& artist) {
        if(song.isEmpty() && artist.isEmpty()) {
          sendRoomMessage("No song found playing on Spotify");
        }
        else {
          sendRoomMessage("Currently playing: " + song + " by " + artist);
        }
      });

  connect(
      _nm.get(),
      &NetworkManager::summoner_id,
      [&](const QString& sid) {
        _summonerIDs.push_back(sid);
        qDebug() << _summonerIDs.size();

        _settings.beginWriteArray("summonerIDs");
        _settings.setArrayIndex(_summonerIDs.size());
        _settings.setValue("id", sid);
        _settings.endArray();

        qDebug() << "added summoner id" << sid;
        sendRoomMessage("successfully added summoner.");
      });

  connect(
      _nm.get(),
      &NetworkManager::runesInfo,
      [&](const QString& runesList) {
        if(runesList.compare("invalid") == 0)
        {
          if (++_requestedId < static_cast<qint32>(_summonerIDs.size() - 1))
          {
            _nm->getSummonerGameInfo(_summonerIDs[_requestedId]);
            return;
          }
          else
            sendRoomMessage("not in an active game.");
        }
        else
          sendRoomMessage(runesList);
        _requestedId = 0;
      });
      

  connect(
      _nm.get(),
      &NetworkManager::rankInfo,
      [&](const QString& rankInfo) {
      	if(!rankInfo.isEmpty())
          sendRoomMessage(rankInfo);
        if (++_rankI < static_cast<qint32>(_summonerIDs.size() - 1))
        {
          _nm->getSummonerRank(_summonerIDs[_rankI]);
          return;
        }
        else
        {
          _rankI = 0;
        }
      });

  connect(
    _nm.get(),
    &NetworkManager::disconnected,
    this,
    &IrcChat::disconnect);

  if(cmd->isSet(CommandLine::channel)) {
    _room = cmd->value(CommandLine::channel);
  }

  loadSettings();

  _authKey = _settings.value("authtoken").toString();
}

IrcChat::~IrcChat() {
  saveSettings();
  for(auto cmd : _repeatCommands) {
    delete cmd;
  }
}

void IrcChat::saveSettings()
{
  for(auto key : _commands.keys()) { 
    _settings.setValue("commands/" + key, _commands[key]);
  }

  _settings.beginWriteArray("repeat");
  int index = 0;
  for(auto key : _repeatCommands.keys()) {
    _settings.setArrayIndex(index);
    _settings.setValue("key", key);
    _settings.setValue("message", _repeatCommands[key]->getMessage());
    _settings.setValue("time", _repeatCommands[key]->getInterval());
    ++index;
  }
  _settings.endArray();

  for(auto user : _authorized) {
    _settings.setValue("user/" + user, true);
  }
}

void IrcChat::loadSettings()
{
  //load commands
  _settings.beginGroup("commands");
  QStringList keys = _settings.allKeys();
  qDebug() << "keys: " << keys;
  for(auto key : keys) { 
    _commands.insert(key, _settings.value(key).toString());
  }
  _settings.endGroup();

  int size = _settings.beginReadArray("repeat");
  for(int i = 0; i < size; ++i) {
    _settings.setArrayIndex(i);
    int time        = _settings.value("time").toInt();
    QString message = _settings.value("message").toString();
    QString key     = _settings.value("key").toString();

    Command* repeat = new Command(time, message);
    qDebug() << time << key << message;

    connect(repeat, &Command::commandReady, this, &IrcChat::sendRepeatedMessage);

    _repeatCommands.insert(key, repeat);
  }
  _settings.endArray();

  _settings.beginGroup("user");
  for(auto user : _settings.allKeys()) {
    _authorized.insert(user);
  }
  _settings.endGroup();

  _settings.beginReadArray("summonerIDs");
  for(int i = 0; i < size; ++i)
  {
    _settings.setArrayIndex(i);
    _summonerIDs.push_back(_settings.value("id").toString());
  }
  _settings.endArray();
}

void IrcChat::join(const QString channel, const QString)
{
  // Join channel's chat room
  sendMessage("JOIN #" + channel);
  //_nm->getTwitchUser("");
}


void IrcChat::leave()
{
  _room = "";
  sendMessage(("PART #" + _room));
}

void IrcChat::disconnect() {
  //tell networkman to d/c emit disconnect signal or something
  _room = "";
  _loggedIn = false;
}

void IrcChat::setAnonymous(bool newAnonymous) {
  if(newAnonymous != _anonym) {
    if(newAnonymous) {
      qsrand(QTime::currentTime().msec());
      _username = "";
      _username.sprintf("justinfan%06d", (qrand() % (1000000 - 100000)) + 100000);
      _userpass = "blah";
    }
    _anonym = newAnonymous;
  }
}

void IrcChat::login()
{
  if (_loggedIn)
    return;

  if (_authKey.isEmpty())
    setAnonymous(true);
  else {
    if(_displayName.isEmpty()) {
      return;
    }
    setAnonymous(false);
  }

  // Tell server that we support twitch-specific commands
  sendMessage("CAP REQ :twitch.tv/commands");
  sendMessage("CAP REQ :twitch.tv/tags");

  // Login
  if(!_anonym)  {
    _username = _displayName;
    _userpass = "oauth:" + _authKey;
  }

  sendMessage("PASS " + _userpass);
  sendMessage("NICK " + _username);

  _loggedIn = true;

  //Join room automatically, if given
  if (!_room.isEmpty()) {
    join(_room, _roomChannelId);
  }
}

/**************************************************
 * 
 *  sendRoomMessage
 * 
 **************************************************/
void IrcChat::sendRoomMessage(const QString& msg)
{
  QString _roomSend = "PRIVMSG #" + _room + " :" + msg;
  sendMessage(_roomSend);
}

/**************************************************
 * 
 *  sendRepeatedMessage
 * 
 **************************************************/
void IrcChat::sendRepeatedMessage(Command* cmd)
{
  if(_messagesSeen >= _messagesInbetween) {
    sendRoomMessage(cmd->getMessage());
    _messagesSeen = 0;
    cmd->resetCommand();
  }
  else {
    cmd->needStatus(true);
  }
}

void IrcChat::checkRepeaters()
{
  Command* cmd = NULL;
  for(auto& repeated : _repeatCommands) {
    if(repeated->checkSendingStatus()) {
      if(cmd) {
        //always try to send longest one first.
        if(cmd->getInterval() < repeated->getInterval()) {
          cmd = repeated;
        }
      }
      else {
        cmd = repeated;
      }
    }
  }
  if(cmd) {
    sendRoomMessage(cmd->getMessage());
    _messagesSeen = 0;
    cmd->resetCommand();
  }
}

void IrcChat::parseCommand(QString cmd)
{
  if(cmd.startsWith("PING ")) {
    sendMessage("PONG");
    return;
  }

  static const QRegExp url("((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[\\-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9\\.\\-]+|(?:www\\.|[\\-;:&=\\+\\$,\\w]+@)[A-Za-z0-9\\.\\-]+)((?:\\/[\\+~%\\/\\.\\w\\-]*)?\\??(?:[\\-\\+=&;%@\\.\\w]*)#?(?:[\\.\\!\\/\\\\\\w]*))?)");

  if(cmd.contains("PRIVMSG")) {
    auto getKey = [](const QString& msg) {
      int startIndex = msg.indexOf(" ")+1;
      int endIndex = msg.indexOf(" ", startIndex);
      return std::make_tuple(msg.mid(startIndex, endIndex - startIndex).toStdString(), endIndex);
    };

    Tags tags(cmd);
    bool privilage = _authorized.contains(tags.getTag("display-name").toLower());

    QString msg = cmd.mid(cmd.indexOf(':', cmd.indexOf("PRIVMSG")) + 1);
    if(msg.startsWith("!song")) {
      _nm->getSpotify().songRequest();
      return;
    }

    if(privilage && msg.startsWith("!addrunes"))
    {
      auto got = getKey(msg);
      QString summonerName = QString::fromStdString(std::get<0>(got));
      _nm->getSummonerID(summonerName);
      return;
    }

    if(msg.startsWith("!runes"))
    {
      if (!_summonerIDs.empty())
      {
        _requestedId = 0;
        _nm->getSummonerGameInfo(_summonerIDs[_requestedId]);
      }
      return;
    }

    if(msg.startsWith("!rank") || msg.startsWith("!elo"))
    {
      if (!_summonerIDs.empty())
      {
        _rankI = 0;
        _nm->getSummonerRank(_summonerIDs[_rankI]);
      }
      return;
    }

    //reserve remove and help
    if(msg.startsWith("!list")) {
      QString msgList;
      for(auto cmd : _commands.keys()) {
        msgList += cmd + " ";
      }
      msgList += " Repeated: <command> <seconds>: ";
      for(auto cmd : _repeatCommands.keys()) {
        msgList += cmd + " " + QString::number(_repeatCommands[cmd]->getInterval() / 1000) + " ";
      }
      sendRoomMessage("commands: " + msgList);
    }
    if(privilage && msg.startsWith("!remove"))
    {
      auto got = getKey(msg);
      QString cmdKey = QString::fromStdString(std::get<0>(got));
      if(_commands.remove(cmdKey) > 0) {
        _settings.remove("commands/" + cmdKey);
        sendRoomMessage("command " + cmdKey + " successfully removed.");
      }
      else if(_authorized.contains(cmdKey.toLower())) {
        _authorized.remove(cmdKey.toLower());
        _settings.remove("user/" + cmdKey.toLower());
        sendRoomMessage(cmdKey + " was successfully removed from authorized users");
      }
      else {
        sendRoomMessage("command " + cmdKey + " does not exist.");
      }
    }

    if(privilage && msg.startsWith("!repeatremove"))
    {
      auto got = getKey(msg);
      QString cmdKey = QString::fromStdString(std::get<0>(got));
      if(_repeatCommands.contains(cmdKey)) {
        _settings.beginWriteArray("repeat");
        int index = 0;
        for(auto key : _repeatCommands.keys()) {
          _settings.setArrayIndex(index);
          if(_settings.value("key").toString() == key) {
            _settings.remove("key");
            _settings.remove("message");
            _settings.remove("time");
            break;
          }
          ++index;
        }
        _settings.endArray();

        delete _repeatCommands[cmdKey];
        _repeatCommands.remove(cmdKey);
        sendRoomMessage("command " + cmdKey + " successfully removed.");
      }
      return;
    }
    if(msg.startsWith("!help")) {
      sendRoomMessage("usage: !add <command name> <command contents> !remove <command name or user name> !auth <user name> !repeat <command name> <interval (seconds)> <command message> !repeatremove <repeated command>");
      return;
    }

    if(privilage && msg.startsWith("!repeat")) {
      auto got = getKey(msg);
      QString cmdKey = QString::fromStdString(std::get<0>(got));
      int startTime = std::get<1>(got);
      int endTime = msg.indexOf(" ", startTime+1);
      QString cmdTime = msg.mid(startTime + 1, endTime - (startTime+1));
      QString cmdMsg = msg.mid(endTime+1);
      if(cmdTime.toInt() < 60 || cmdMsg.isEmpty()) {
        sendRoomMessage("please make sure the time is more than 60 and that the message is not empty.");
        return;
      }

      Command* repeat = new Command(cmdTime.toInt() * 1000, cmdMsg);

      connect(repeat, &Command::commandReady, this, &IrcChat::sendRepeatedMessage);

      _repeatCommands.insert(cmdKey, repeat);

      sendRoomMessage("repeat command " + cmdKey + " successfully added!");
      return;
    }

    if(privilage && msg.startsWith("!add")) {
      auto got = getKey(msg);
      QString cmdKey = QString::fromStdString(std::get<0>(got));
      if(!_commands.contains(cmdKey) && cmdKey != "add") {
        QString cmdMsg = msg.mid(std::get<1>(got) + 1);
        _commands.insert(cmdKey, cmdMsg);

        sendRoomMessage("command " + cmdKey + " successfully added.");
      }
      else {
        if(cmdKey == "add") {
          sendRoomMessage("can not add \"add\" as a command.");
          return;
        }

        sendRoomMessage(cmdKey + " already exists, please remove first.");
      }
      return;
    }

    if(privilage && msg.startsWith("!auth")) {
      auto got = getKey(msg);
      QString cmdUser = QString::fromStdString(std::get<0>(got));
      _authorized.insert(cmdUser.toLower());
      sendRoomMessage(cmdUser + " was successfully added to the authorized list");
      return;
    }

    if((tags.getTag("badges").contains("broadcaster") || !tags.getTag("user-type").isEmpty()) && msg.startsWith("!permit"))
    {
      auto got = getKey(msg);
      QString cmdUser = QString::fromStdString(std::get<0>(got));
      cmdUser = cmdUser.toLower();
      _permittedUsers.insert(cmdUser);
      QTimer* timer = new QTimer();
      timer->setSingleShot(true);
      timer->setInterval(180000);
      connect(timer, &QTimer::timeout, [this, timer, cmdUser]() {
          _permittedUsers.remove(cmdUser);
          qDebug() << "remove user: " << cmdUser;
          timer->deleteLater();
      });
      sendRoomMessage(cmdUser + " is allowed to post a link for 3 mins.");
      timer->start();
      return;
    }


    if(msg.startsWith('!') && !msg.startsWith("!add") && !msg.startsWith("!remove"))
    {
      QString command = msg.mid(1, msg.indexOf(" "));
      if(_commands.contains(command)) {
        sendRoomMessage(_commands[command]);
      }
      return;
    }

    //only increase messages seen if it's not a command message
    ++_messagesSeen;
    if(_messagesSeen > _messagesInbetween) {
      checkRepeaters();
    }

    if(url.indexIn(msg) != -1)
    {
      if(QString sub = tags.getTag("subscriber");
          !sub.toInt() && tags.getTag("user-type").isEmpty() && !_permittedUsers.contains(tags.getTag("display-name").toLower()))
      {
        sendRoomMessage("/timeout " + tags.getTag("display-name") + " 10");
        sendRoomMessage("@" + tags.getTag("display-name") + " smiteRage No links you pleb (Warning) smiteRage.");
      }
    }

    qDebug() << cmd;
    return;
  } // privmsg

  if(cmd.contains("USERNOTICE")) {
    Tags tags(cmd);

    QString type = tags.getTag("msg-id");

    if(type == "sub" || type == "resub") {
      QString username = tags.getTag("display-name");
      if(type == "sub") {
        sendRoomMessage(QString("smiteOld smiteOld smiteOld Welcome To the Retirement home, %1! A NEW BLOOD! smiteRage smitePog").arg(username));
      }
      else if(tags.getTag("msg-param-should-share-streak").toInt())
      {
        QString months = tags.getTag("msg-param-streak-months");
        if (months.toInt() < 10)  {
          sendRoomMessage(QString("smiteOld smiteOld smiteOld Welcome To the Retirement home, %1! %2 months old sub smiteTilt smiteDog!").arg(username).arg(months));
        }
        else if(months.toInt() < 30) {
          sendRoomMessage(QString("smiteOld smiteOld smiteOld Welcome To the Retirement home, %1! Our LOYAL sub is %2 months old smiteGood smiteLaugh!").arg(username).arg(months));
        }
        else {
          sendRoomMessage(QString("smiteOld smiteOld smiteOld Welcome To the Retirement home, %1! OLDIES BUT GOLDIES %2 years with us smiteFiesta smiteFiesta!").arg(username).arg(months));
        }
      }
      else {
        QString months = tags.getTag("msg-param-cumulative-months");
        sendRoomMessage(QString("smiteOld smiteOld smiteOld Welcome To the Retirement home, %1! %2 month(s) old sub smiteTilt smiteDog!").arg(username).arg(months));
      }
    }

    if(type == "subgift") {
      QString gifter = tags.getTag("display-name");
      QString recipient = tags.getTag("msg-param-recipient-display-name");
      QString months = tags.getTag("msg-param-cumulative-months");
      sendRoomMessage(QString("smiteOld smiteOld smiteOld Welcome to the Retirement home %1! Don't forget to thank your sugar daddy %2 smiteLaugh").arg(recipient).arg(gifter));
    }
  }

  qDebug() << cmd;
  return;
}
