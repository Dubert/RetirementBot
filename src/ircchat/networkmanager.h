#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QSettings>
#include <QSslSocket>
#include <QNetworkAccessManager>
#include <QTimer>

#include "httpserver.h"
#include "spotify.h"

#include <memory>

enum class UserType
{
  AuthUser,
  StreamUser,
  OtherUser,
};

class NetworkManager : public QObject
{
  Q_OBJECT

  std::unique_ptr<QSslSocket> _sock;

  //server to get auth token
  HttpServer _server;
  QSettings _settings;
  Spotify _spotify;
  QString _authKey;
  bool _receivedPong = false;
  std::unique_ptr<QTimer> sendPing;
  QString _riotVersion;
  QString _requestedId;

  QMap<int, QString> _runes;

  QNetworkAccessManager man;
  QTimer offlinePoller;
  bool connectionOK = false;

  static const quint16 PORT;
  static const QString HOST;
  static const QString Kraken;
  static const QString CLIENT_ID;
  static const QString RiotGames;
  static const QString summoner_v4;
  static const QString spectator_v4;
  static const QString league_v4;
  static const QString static_data_v3;

private slots:
  void receive();
  void processError(QAbstractSocket::SocketError socketError);
  void processSslErrors(const QList<QSslError> &errors);
  void testConnection();
  void testConnectionReply();

  void userReply();
  void twitchUsersReply();
  void twitchStreamReply();
  void summonerReply();
  void gameInfoReply();
  void rankInfoReply();
  void riotVersionReply();
  void runesReply();

signals:
  void finishedConnectionTest();
  void recievedMessage(const QString& msg);
  void recievedUserInfo(const QString& displayName, quint64 userId, UserType type);
  void recievedStreamUptime(qint64 seconds);
  void login();
  void runesInfo(const QString& runes);
  void rankInfo(const QString& runes);
  void summoner_id(const QString& id);
  void disconnected();

public slots:
  void onSocketStateChanged();
  void sendMessage(const QString& msg);

public:
  NetworkManager(QObject* parent = 0);
  ~NetworkManager();

  void getSummonerID(const QString& summonerName);
  void getSummonerGameInfo(const QString& summonerID);
  void getSummonerRank(const QString& summonerID);
  void requestUserStream(quint64 id);
  void getTwitchUser(const QString& users);

  const Spotify& getSpotify() const {
    return _spotify;
  }

private:
  bool connected();
  void disconnect();
  void send_ping();
  void reopenSocket();
  void getUser();
  void getRiotVersionInfo();
  void getRuneInfo();
  void testNetworkInterface();
  bool handleNetworkError(QNetworkReply* reply);
};

#endif //NETWORKMANAGER_H
