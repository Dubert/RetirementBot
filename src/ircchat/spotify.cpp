#include "spotify.h"
#include "client_info.h"

#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QNetworkRequest>
#include <QNetworkReply>

#include <QTimer>

const QString Spotify::SpotifyToken = "https://accounts.Spotify.com/api/token";

Spotify::Spotify(QSettings* settings, QNetworkAccessManager* man, QObject* parent) :
  ApiBase(settings, man, parent)
{
  if (_refreshToken = _settings->value("Spotifytoken").toString();
   !_refreshToken.isEmpty())
  {
    refreshRequest("refresh_token="+_refreshToken, "refresh_token");
  }
}

/********************************************************************
 *
 *  keyExpired()
 *
 ********************************************************************/
void Spotify::keyExpired()
{
  refreshRequest("refresh_token="+_refreshToken, "refresh_token");
}

/********************************************************************
 *
 *  refreshRequest()
 *
 ********************************************************************/
void Spotify::refreshRequest(const QString& key, const QString& type)
{
  QNetworkRequest request;
  request.setUrl(QUrl(SpotifyToken));
  request.setRawHeader(QString("Authorization").toUtf8(), QString("Basic ").toUtf8() + QString(SPOTIFY_ID ":" SPOTIFY_SECRET).toUtf8().toBase64());

  _data = std::make_unique<QByteArray>(
      QString("grant_type="+type+"&"+key).toStdString().c_str());

  request.setRawHeader("Content-Length", QByteArray::number(_data->size()));

  QNetworkReply *reply = _man->post(request, *_data);

  connect(
      reply,
      &QNetworkReply::finished,
      this,
      &Spotify::receivedRefreshToken);
}

/********************************************************************
 *
 *  songRequest()
 *
 ********************************************************************/
void Spotify::songRequest() const
{
  // do not request if there is no access token.
  if (_accessToken.isEmpty())
  {
    return;
  }

  QNetworkRequest request;
  request.setUrl(QUrl("https://api.Spotify.com/v1/me/player/currently-playing"));
  request.setRawHeader(QString("Authorization").toUtf8(), QString("Bearer ").toUtf8() + _accessToken.toUtf8());

  QNetworkReply *reply = _man->get(request);

  connect(
      reply,
      &QNetworkReply::finished,
      this,
      &Spotify::receivedSong);
}

/********************************************************************
 *
 *  receivedSong()
 *
 ********************************************************************/
void Spotify::receivedSong()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data, &error);

  QString songName;
  QString artist;
  if (error.error == QJsonParseError::NoError) {
    QJsonObject json = doc.object();
    if (!json["item"].isNull()) {
      songName = json["item"].toObject()["name"].toString();
      artist = json["item"]
                .toObject()["artists"]
                .toArray().at(0).toObject()["name"].toString();
    }
  }
  emit songInfo(songName, artist);

  reply->deleteLater();
}

/********************************************************************
 *
 *  receivedRefreshToken()
 *
 ********************************************************************/
void Spotify::receivedRefreshToken()
{
  QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

  QByteArray data = reply->readAll();

  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(data, &error);

  if (error.error == QJsonParseError::NoError) {
    QJsonObject json = doc.object();
    if (!json["access_token"].isNull()) {
      _accessToken = json["access_token"].toString();
    }
    if(!json["expires_in"].isNull()) {
      qDebug() << "Expires in: " << json["expires_in"].toString();
      //request new token when this one expires
      QTimer::singleShot(
          json["expires_in"].toInt() * 1000,
          this, &Spotify::keyExpired);
    }
    if (!json["refresh_token"].isNull()) {
      _refreshToken = json["refresh_token"].toString();
      _settings->setValue("Spotifytoken", _refreshToken);
    }
    qDebug() << json;
  }
  qDebug() << "access " << _accessToken << "refresh" << _refreshToken;

  reply->deleteLater();
}
