#include "commandline.h"

const QCommandLineOption CommandLine::channel(
    "channel", "what irc chat channel to connect to.", "channel name");
const QCommandLineOption CommandLine::user_name(
    "user_name", "account name to log into", "string");
const QCommandLineOption CommandLine::password(
    "password", "password for the account", "string");

static CommandLine* commandline = nullptr;

CommandLine::CommandLine(QCoreApplication* app)
{
  _parser.addOption(channel);
  _parser.addOption(user_name);
  _parser.addOption(password);

  _parser.process(*app);

  commandline = this;
}

CommandLine::~CommandLine()
{
  commandline = nullptr;
}

CommandLine& CommandLine::instance()
{
  return *commandline;
}

bool CommandLine::isSet(const QCommandLineOption& option) const
{
  return _parser.isSet(option);
}

QString CommandLine::value(const QCommandLineOption& option) const
{
  return _parser.value(option);
}
